from django.shortcuts import render, redirect
from .forms import UserModelForm
from .models import UserModel
from django.contrib.auth.models import User
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
    return render(request, 'web/index.html', {})

def user_logout(request):
    logout(request)
    return redirect('/')  

def register(request):
    if request.method == 'POST':
        form = UserModelForm(request.POST)
        if form.is_valid():
            new_user = User.objects.create_user(
                username=form.cleaned_data['username'],
                email=form.cleaned_data['email'],
                password=form.cleaned_data['password1']  
            )
            new_profile = UserModel.objects.create(
                user=new_user,
                role=form.cleaned_data['role'],
                country=form.cleaned_data['country'],
                nationality=form.cleaned_data['nationality'],
                mobile=form.cleaned_data['mobile']
            )

            return redirect('login') 
    else:
        form = UserModelForm()
    return render(request, 'web/registration.html', {'form': form})
