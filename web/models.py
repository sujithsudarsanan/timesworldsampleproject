from django.db import models
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser)
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import User

class UserModel(models.Model):
	ROLE_CHOICES = [
		('student', 'Student'),
		('staff', 'Staff'),
		('admin', 'Admin'),
		('editor', 'Editor'),
	]
	username = models.CharField(('username'),
		max_length=150,
		unique=True,
		help_text=('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
		error_messages={
		'unique': ("A user with that username already exists."),
	},
	)
	email = models.EmailField(
		verbose_name='email address',
		max_length=255,
		unique=True,
	)
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	role = models.CharField(max_length=20, choices=ROLE_CHOICES)
	passsword = models.CharField(max_length=250,default='')
	country = models.ForeignKey('cities_light.Country', on_delete=models.SET_NULL, null=True, blank=True) 
	nationality = models.CharField(max_length=100,verbose_name='nationality',)
	mobile = models.CharField(max_length=15,verbose_name='mobile number',)
	EMAIL_FIELD = 'email'
	USERNAME_FIELD = 'username'
	REQUIRED_FIELDS = ['email']