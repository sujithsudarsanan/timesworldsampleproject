from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import UserModel


class UserModelForm(UserCreationForm):
    ROLE_CHOICES = [
        ('student', 'Student'),
        ('staff', 'Staff'),
        ('admin', 'Admin'),
        ('editor', 'Editor'),
    ]
    role = forms.ChoiceField(choices =ROLE_CHOICES)
    nationality = forms.CharField(max_length=100)
    mobile = forms.CharField(max_length=15)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.widget.attrs['placeholder'] = visible.field.label

    class Meta:
        model = UserModel
        fields = ['username', 'email', 'role', 'country', 'nationality', 'mobile', 'password1', 'password2']