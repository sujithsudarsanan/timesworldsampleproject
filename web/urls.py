# coding=utf-8
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'), 
    path('register/', views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='web/login.html'), name='login'),
    path('user_logout/', views.user_logout, name='user_logout'),
    path('accounts/profile/', auth_views.LoginView.as_view(template_name='web/dashboard.html'), name='accounts/profile'),
]